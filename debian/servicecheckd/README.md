Servicecheckd is script that periodically checks service existance at specific

address (typically bound locally at server where it runs) and generates filters

(filter1.conf...filterN.conf) bird routing daemon. When new snippet generated,

it automatically reloads bird.


Servicecheckd originally based on resolvcheckd which checks only dns service,

now it checks either dns or http(s) services.
