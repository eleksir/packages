#!/usr/bin/env perl

use warnings "all";
use strict;

use JSON::PP;
use DNS::ZoneParse;
use Fcntl;

sub process_zone(@);

my $conf_file   = '/etc/bind2unbound.json';
my $unboundzone = '/etc/unbound/unbound.zone';

open (CONF, "$conf_file") || die "unable to read $conf_file";
my $sep = $/; undef $/;
my $conf = <CONF>;
close CONF;
$/ = $sep; undef $sep;

$conf = decode_json $conf;
my %CONF = %$conf;
undef $conf;

my $zonedata = '';

foreach my $zone_name (keys(%CONF)){
        $zonedata .= "local-zone: $zone_name typetransparent\n";
        $zonedata .= process_zone($zone_name, $CONF{$zone_name});
}

sysopen (UNBOUNDZONE, $unboundzone, O_CREAT|O_TRUNC|O_WRONLY ) || die "unable to find $unbound.zone file";
syswrite UNBOUNDZONE, $zonedata;
close UNBOUNDZONE;

exit 0;


sub process_zone(@) {
        my ($name, $file) = @_;
        my $namelen = length($name);
        my $zonefile = DNS::ZoneParse->new($file, $name);
        my $zone = $zonefile->dump; # ref to hash
        my $data = '';

        foreach my $record(@{${$zone}{NS}}){
                next if ($record->{name} =~ /[#|\*|\@]/);

                if ( ! ((length($record->{name}) > $namelen) and (substr($record->{name}, 0-$namelen) eq $name))) {
                        $record->{name} .= ".$name";
                }

                if ((substr($record->{host}, -1) ne '.') and ( ! ((length($record->{host}) > $namelen) and (substr($record->{host}, 0-$namelen) eq $name)))) {
                        $record->{host} .= ".$name";
                }

                $data .= "\tlocal-data: \"$record->{name} $record->{ttl} $record->{class} NS $record->{host}\"\n";
        }

        foreach my $record(@{${$zone}{A}}){
                next if ($record->{name} =~ /[#|\*|\@]/);

                if ( ! ((length($record->{name}) > $namelen) and (substr($record->{name}, 0-$namelen) eq $name))) {
                        $record->{name} .= ".$name";
                }

                $data .= "\tlocal-data: \"$record->{name} $record->{ttl} $record->{class} A $record->{host}\"\n";
        }

        foreach my $record(@{${$zone}{AAAA}}){
                next if ($record->{name} =~ /[#|\*|\@]/);

                if ( ! ((length($record->{name}) > $namelen) and (substr($record->{name}, 0-$namelen) eq $name))) {
                        $record->{name} .= ".$name";
                }

                $data .= "\tlocal-data: \"$record->{name} $record->{ttl} $record->{class} AAAA $record->{host}\"\n";
        }

        foreach my $record(@{${$zone}{CNAME}}){
                next if ($record->{name} =~ /[#|\*|\@]/);

                if ( ! ((length($record->{name}) > $namelen) and (substr($record->{name}, 0-$namelen) eq $name)) ) {
                        $record->{name} .= ".$name";
                }

                if ((substr($record->{host}, -1) ne '.') and ( ! ((length($record->{host}) > $namelen) and (substr($record->{host}, 0-$namelen) eq $name)))) {
                        $record->{host} .= ".$name";
                }

                $data .= "\tlocal-data: \"$record->{name} $record->{ttl} $record->{class} CNAME $record->{host}\"\n";
        }

        return $data;
}

__END__
