#!/usr/bin/perl
#
# prints SHA256 Signatures for packages in openbsd
# packaging format


use strict;
use warnings "all";
use Digest::SHA qw(sha256_base64);

sub help;
sub hash_str;

help unless defined($ARGV[0]);

opendir DIR, $ARGV[0] or die;
my $file;

while ($file = readdir(DIR)){
        if($file =~ /\.tgz$/){
                hash_str($file);
        }
}

exit 0;


sub hash_str {
        my $file = shift;
        open my $fh, '<', $file or die;
        $/ = undef;
        my $data = <$fh>;
        close $fh;
        print "SHA256 ($file) = " . sha256_base64($data) . "=\n";
}


sub help {
        print "Usage: $0 directory\n";
        exit 0;
}
