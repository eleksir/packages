#!/bin/bash

# OpenBSD mirror script for openbsd-5.8 amd64
#
# 23.03.2016 - Fedosov S. - initial revision

OS="openbsd"
REL="5.8"
ARCH="amd64"
MIRROR_ROOT="/srv/mirrors"

DIR="${MIRROR_ROOT}/${OS}/${REL}"

mkdir -p "$DIR"
cd "$DIR"

rsync -v --progress rsync://mirror.yandex.ru/${OS}/${REL}/* .
rsync -av --progress rsync://mirror.yandex.ru/${OS}/${REL}/${ARCH} .

mkdir packages
cd packages

rsync -av --progress rsync://mirror.yandex.ru/${OS}/${REL}/packages/${ARCH} .
