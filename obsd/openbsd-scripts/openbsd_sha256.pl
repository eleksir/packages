#!/usr/bin/perl
#
# prints sha256 signature for given file

use strict;
use warnings "all";
use Digest::SHA qw(sha256_base64);

sub help;

help unless defined($ARGV[0]);


my $file = $ARGV[0];

open my $fh, '<', $file or die;
$/ = undef;
my $data = <$fh>;
close $fh;
print sha256_base64($data) . "=\n";

sub help {
        print "Usage: $0 file\n";
        exit 0;
}
