#!/usr/bin/perl

# this script writes /etc/sudoers.d/sudoers from sudoUser attributes of AD record

# 13 apr 2016 - initial release

use strict;
use warnings "all";

use Fcntl;
use Net::LDAP;
use AppConfig;

sub ldapassert(@);

my $config = AppConfig->new();
$config->define(
	"ldapserver" => { ARGCOUNT => AppConfig::ARGCOUNT_ONE },
	"basedn"     => { ARGCOUNT => AppConfig::ARGCOUNT_ONE },
	"binddn"     => { ARGCOUNT => AppConfig::ARGCOUNT_ONE },
	"bindpw"     => { ARGCOUNT => AppConfig::ARGCOUNT_ONE },
	"sudoers"    => { ARGCOUNT => AppConfig::ARGCOUNT_ONE },
);

$config->file('/etc/ldap-sudoers-sync.conf');

my $server   = $config->get("ldapserver");
my $login    = $config->get('binddn');
my $password = $config->get('bindpw');
my $base     = $config->get('sudoers');
my @usedidlist;

my $ldap = Net::LDAP->new( $server ) ||  die $@;
my $result = ldapassert($ldap->bind(dn => $login, password => $password),  "binding to the server");

my $mesg = $ldap->search(
	filter=>"(objectClass=*)",
	base=>$base,
	scope=>"sub"
) ||  die $@;

my @entries = $mesg->entries;
# list of users stored as list of distiguished names, so extract them
sysopen(SUDOERS, "/etc/sudoers.d/sudoers", O_CREAT|O_TRUNC|O_RDWR, 0600) || die;
sysopen(DOASCONF, "/etc/doas.conf", O_CREAT|O_TRUNC|O_RDWR, 0600) || die;
foreach ($entries[0]->get_value('sudoUser')){
	syswrite SUDOERS, "$_ ALL=(ALL) NOPASSWD: ALL\n";
	syswrite DOASCONF, "permit nopass keepenv { ENV PS1 SSH_AUTH_SOCK } $_\n";
}
close SUDOERS;
close DOASCONF;

@entries = -1;

$ldap->unbind;

exit 0;

sub ldapassert(@) {
	my $mesg = shift;
	my $action = shift;
	if ($mesg->code) {
		die "An error occurred $action: ".ldap_error_text($mesg->code)."\n";
	}
	return $mesg;
}
