pkg_create \
        -A \* \
        -B `pwd` \
        -p / \
        -D MAINTAINER='Sergei Fedosov <sfedosov@masterhost.ru>' \
        -D FULLPKGPATH=sysutils/ldap-sudoers-sync-mh \
        -D COMMENT="sudo ldap sync script" \
        -D HOMEPAGE="https://masterhost.ru" \
        -d README \
        -f plist.meta -f plist ../ldap-sudoers-sync-mh-1.0.0.tgz
