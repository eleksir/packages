pkg_create \
        -A \* \
        -B `pwd` \
        -p / \
        -D MAINTAINER='Sergei Fedosov <sfedosov@masterhost.ru>' \
        -D FULLPKGPATH=sysutils/bind2unbound \
        -D COMMENT="Bind zone-file parcer for unbound resolver." \
        -D HOMEPAGE="https://masterhost.ru/" \
        -d README \
        -f plist.meta -f plist ../bind2unbound-mh-1.0.0.tgz
